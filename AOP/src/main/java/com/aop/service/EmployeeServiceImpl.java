package com.aop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aop.dao.EmployeeRepository;
import com.aop.model.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository empRepo;

	@Override
	public void insertEmployee(Employee emp) {
		empRepo.save(emp);

	}

	@Override
	public Employee updateEmployee(int empid, Employee emp) {

		return empRepo.save(emp);
	}

	@Override
	public void deleteEmployee(int id) {

		empRepo.deleteById(id);

	}

	@Override
	public Employee getEmployees(int empId) {

		return empRepo.findById(empId).get();
	}

	@Override
	public List<Employee> getAllEmployees() {
		return (List<Employee>) empRepo.findAll();
	}

}
