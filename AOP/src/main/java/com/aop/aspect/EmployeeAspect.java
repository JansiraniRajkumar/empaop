package com.aop.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.aop.model.Employee;

@Aspect
@Component
public class EmployeeAspect {

	@Before(value = "execution(* com.aop.service.EmployeeService.*(..)) and args(empid,emp)")
	public void beforeAdvice(JoinPoint joinPoint, int empid, Employee emp) {
		System.out.println("Before method:" + joinPoint.getSignature());
		System.out.println("Updating Employee with employee Id - " + empid + ", employee name - "
				+ emp.getEmployeeName() + ",  address - " + emp.getAddress() + ", email -  " + emp.getEmail()
				+ ", Phone number -  " + emp.getPhone() + " and Project Name -  " + emp.getProjectName());

	}

	@After(value = "execution(* com.aop.service.EmployeeService.*(..)) and args(empid,emp)")
	public void afterAdvice(JoinPoint joinPoint, int empid, Employee emp) {
		System.out.println("After method:" + joinPoint.getSignature());
		System.out.println("Updating Employee with employee Id - " + empid + ", employee name - "
				+ emp.getEmployeeName() + ",  address - " + emp.getAddress() + ", email -  " + emp.getEmail()
				+ ", Phone number -  " + emp.getPhone() + " and Project Name -  " + emp.getProjectName());

	}

	@AfterReturning(value = "execution(* com.aop.service.EmployeeService.*(..)) and args(empid,emp)", returning = "result")
	public void afterReturning(JoinPoint joinPoint, int empid, Employee emp, Object result) {
		System.out.println("Method : " + joinPoint.getSignature() + " result : " + result);
	}

	@After(value = "execution(* com.aop.service.EmployeeService.*(..)) and args(emp)")
	public void afterAdvice(JoinPoint joinPoint, Employee emp) {
		System.out.println("After method:" + joinPoint.getSignature());
		System.out.println("Creating Employee with employee Id - " + emp.getEmployeeId() + ", employee name - "
				+ emp.getEmployeeName() + ",  address - " + emp.getAddress() + ", email -  " + emp.getEmail()
				+ ", Phone number -  " + emp.getPhone() + " and Project Name -  " + emp.getProjectName());

	}

	@Before(value = "execution(* com.aop.service.EmployeeService.*(..)) and args(emp)")
	public void beforeAdviceGetEmployee(JoinPoint joinPoint, Employee emp) {
		System.out.println("Before method:" + joinPoint.getSignature());
		System.out.println("Creating Employee with employee Id - " + emp.getEmployeeId() + ", employee name - "
				+ emp.getEmployeeName() + ",  address - " + emp.getAddress() + ", email -  " + emp.getEmail()
				+ ", Phone number -  " + emp.getPhone() + " and Project Name -  " + emp.getProjectName());
	}

	@Around(value = "execution(* com.aop.service.EmployeeService.*(..)) and args(emp))")
	public void aroundAdvice(ProceedingJoinPoint joinPoint, Employee emp) throws Throwable {

		joinPoint.proceed();
		System.out.println("Around method: " + joinPoint.getSignature());

	}

	@Pointcut(value = "execution(* com.aop.service.EmployeeService.getEmployees(..))")
	private void getEmployeesById() {
	}

	@AfterThrowing(value = "getEmployeesById()", throwing = "exception")
	public void afterThrowingAdvice(JoinPoint jp, Throwable exception) {
		System.out.println("Inside afterThrowingAdvice() method : " + jp.getSignature().getName() + " method");
		System.out.println("Exception= " + exception);
	}

}
