package com.aop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.aop.model.Employee;
import com.aop.service.EmployeeService;

@RestController
public class EmployeeController {

	@Autowired
	private EmployeeService empService;

	@GetMapping("/listEmp")
	public List<Employee> getAllEmployees() {
		return empService.getAllEmployees();
	}

	@GetMapping("/listEmp/{id}")
	public Employee getEmployeeById(@PathVariable int id) {
		return empService.getEmployees(id);
	}

	@PostMapping("/addEmp")
	public Employee addEmployee(@RequestBody Employee employee) {
		empService.insertEmployee(employee);
		return employee;
	}

	@PutMapping("/updateEmp/{id}")
	public Employee updateEmployee(@RequestBody Employee employee, @PathVariable int id) {

		empService.updateEmployee(id, employee);
		return employee;
	}

	@DeleteMapping("/deleteEmp/{id}")
	public String deleteEmployee(@PathVariable int id) {
		empService.deleteEmployee(id);
		return "Record Deleted";

	}

}
